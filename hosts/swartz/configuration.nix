{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ../../modules/common
  ];

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;

  # for LISH
  boot.kernelParams = [ "console=ttyS0,19200n8" ];
  boot.loader.grub.extraConfig = ''
    serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
    terminal_input serial;
    terminal_output serial;
  '';
  boot.loader.grub.forceInstall = true;
  boot.loader.grub.device = "nodev";
  boot.loader.timeout = 10;
  
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
  };

  networking.usePredictableInterfaceNames = false;
  networking.hostName = "swartz";
  time.timeZone = "Europe/Dublin";

  networking.interfaces.eth0.useDHCP = true;

  i18n.defaultLocale = "en_IE.UTF-8";

  users.users.osslate = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ];
    openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPMj62Wdnw9Ij21sML3avK30cInsr2RQRSnBneaKW1/s osslate@thinkpad" ];
  };

  environment.systemPackages = with pkgs; [
    inetutils
    mtr
    sysstat
    git
  ];

  nix.gc = {
    automatic = true;
    dates = "weekly";
  };

  services.radicale = {
    enable = true;
    config = ''
      [server]
      hosts = 0.0.0.0:5252
    '';
  };

  networking.firewall.enable = false;

  system.stateVersion = "21.05";
}