# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;

  imports = [
    ./hardware-configuration.nix
    ../../modules/common
    ../../modules/desktop
    ../../overlays.nix
  ];

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "thinkpad";
  time.timeZone = "Europe/Dublin";

  networking.interfaces.wlp0s20f3.useDHCP = true;

  i18n.defaultLocale = "en_IE.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  services.printing.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.osslate = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "networkmanager" "adb" ];
  };

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    firefox
    local.pop-shell
    gnome.gnome-tweaks
    gnome.pomodoro
    gnomeExtensions.caffeine
    gnomeExtensions.dynamic-panel-transparency
    gnomeExtensions.appindicator
    pulseeffects-pw
  ];

  programs.dconf.enable = true;

  programs.adb.enable = true;

  services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];
  services.gnome.sushi.enable = true;

  hardware.cpu.intel.updateMicrocode = true;

  /*
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };*/

  services.openssh = {
    enable = true;
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  system.stateVersion = "21.11"; # Did you read the comment?
}

