{ lib, ... }:

{
  nixpkgs.overlays = [
    (self: super: {
      local = import ./local-pkgs {
        pkgs = self;
        inherit lib;
      };
    })
  ];
}