{ config, pkgs, lib, ...}:

{
  nix.gc = {
    automatic = true;
    dates = "weekly";
  };
}