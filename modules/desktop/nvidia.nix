{ config, pkgs, lib, ...}:

{
  # use 470 to test
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.beta;

  # modesetting is required in order to use Wayland, and for whatever reason,
  # it's disabled by NVIDIA's proprietary drivers by default
  hardware.nvidia.modesetting.enable = true;

  # explicitly use NVIDIA drivers in X and Wayland
  # by default, GDM blacklists NVIDIA's drivers, but these days they work fine.
  # the blacklist will go following driver 470, maybe??
  services.xserver = {
    videoDrivers = [ "nvidia" ];
    #displayManager.gdm.nvidiaWayland = true;
  };
}