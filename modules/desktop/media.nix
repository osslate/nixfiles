{ config, pkgs, lib, ...}:

{
  # We're using PipeWire to handle PulseAudio and JACK applications.
  # We can disable it here.
  hardware.pulseaudio.enable = false;
  # Helps improve latency, etc. on a non-realtime kernel
  # ...or if I'm honest, I'm not entirely sure.
  security.rtkit.enable = true;

  # enable pipewire to handle PA/JACK apps.
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    config.pipewire = {
      "context.properties" = {
        "default.clock.quantum" = 256;
        "default.clock.min-quantum" = 256;
        "default.clock.max-quantum" = 256;
      };
      "context.modules" = [
        {
          name = "libpipewire-module-rtkit";
          args = {
            "nice-level" = -15;
            "rt.prio" = 88;
            "rt.time.soft" = 200000;
            "rt.time.hard" = 200000;
          };
          flags = [ "ifexists" "nofail" ];
        }
        { name = "libpipewire-module-protocol-native"; }
        { name = "libpipewire-module-profiler"; }
        { name = "libpipewire-module-metadata"; }
        { name = "libpipewire-module-spa-device-factory"; }
        { name = "libpipewire-module-spa-node-factory"; }
        { name = "libpipewire-module-client-node"; }
        { name = "libpipewire-module-client-device"; }
        {
          name = "libpipewire-module-portal";
          flags = [ "ifexists" "nofail" ];
        }
        {
          name = "libpipewire-module-access";
          args = {};
        }
        { name = "libpipewire-module-adapter"; }
        { name = "libpipewire-module-link-factory"; }
        { name = "libpipewire-module-session-manager"; }
      ];
    };
    media-session.config.alsa-monitor = {
      rules = [
        {
          matches = [ { "node-name" = "alsa-output.*"; } ];
          actions = {
            update-props = {
              "audio.rate" = 96000;
              "api.alsa.period-size" = 6;
              "api.alsa.disable-batch" = true;
            };
          };
        }
      ];
    };
  };
}