{ config, pkgs, lib, ...}:

{
  environment.systemPackages = with pkgs; [ xwayland xclip ];  

  services.xserver = {
    enable = true;
    displayManager.gdm = {
      enable = true;
      #wayland = true;
    };
    desktopManager.gnome = {
      enable = true;
      extraGSettingsOverrides = ''
        [org.gnome.desktop.interface]
        gtk-theme='Adwaita-dark'
        [org.gnome.desktop.wm.preferences]
        button-layout=":minimize,maximize,close"
      '';
    };
  };

  programs.gnome-terminal.enable = true;
  programs.gpaste.enable = true;

  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";
}