{ config, pkgs, lib, ...}:

{
  imports = [
    ./gnome.nix
    ./nvidia.nix
    ./media.nix
  ];

  # enable Flatpak support for sandboxed desktop apps
  services.flatpak.enable = true;

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    firefox
    nix-prefetch-git
  ];
}