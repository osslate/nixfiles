# Based on a PR here: https://github.com/NixOS/nixpkgs/pull/102150
#
# Original derivation created in 2020 by Patrick Hilhorst & licensed under the
# MIT License.

{ stdenv, lib, fetchFromGitHub, glib, nodePackages }:

stdenv.mkDerivation rec {
  pname = "gnome-shell-extension-pop-shell";
  version = "b33c5e260d1feda39de442cef9a222105e972932";

  src = fetchFromGitHub {
    owner = "pop-os";
    repo = "shell";
    rev = version;
    sha256 = "0g3as2vjd81mb3bap8qha1fwlm1q8jj4y3sgqx13k7acgznkikdx";
  };

  makeFlags = [ "DESTDIR=$(out)" ];
  buildInputs = [ glib nodePackages.typescript ];

  postInstall = ''
    mv $out/usr/* $out
    rmdir $out/usr
  '';

  meta = with lib; {
    description = "A tiling extension for the GNOME Shell";
    longDescription = ''
      A keyboard-driven layer for GNOME Shell which allows for quick and
      sensible navigation and management of windows. The core feature of Pop
      Shell is the addition of advanced tiling window management.
    '';
    license = licenses.gpl3;
    maintainers = with maintainers; [ ];
    homepage = src.meta.homepage;
  };
}
